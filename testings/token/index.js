const axios = require("axios");
const fs = require("fs");
const path = require("path");

const ax = axios.create({
  headers: {},
});

const getFileContent = (token) => `
const placeHolderData2 = [
    { value: "chocolate", label: "c" },
    { value: "strawberry", label: "${token}" },
    { value: "vanilla", label: "Vanilla" },
  ];
  
`;

let interval;

const init = async () => {
  try {
    const { data } = await axios.get("http://google.com", {
      headers: {
        Cookie: "smsession=value;",
      },
    });
    console.log(data);
    const { jwt } = data;
    if (!jwt)
      throw new Error(
        "\n\n\nJWT fetch failed, Check network or update your cookie"
      );
    const content = getFileContent(jwt);
    fs.writeFileSync(path.join(__dirname, "some.js"), content);
  } catch (e) {
    console.error(e);
    clearInterval(interval);
    process.exit();
  }
};

interval = setInterval(init, 10 * 60 * 1000); // 10 minutes
init();

const { Sequelize, QueryTypes } = require("sequelize");

const connString = "postgres://user:pass@example.com:5432/dbname";
const sequelize = new Sequelize(connString, {
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
});

const init = async () => {
  try {
    await sequelize.authenticate();
    console.log("Connection has been established successfully.");
  } catch (error) {
    console.error("Unable to connect to the database:", error);
  }
};

const fetchData = async () => {
  try {
    const users = await sequelize.query(
      "SELECT * FROM projects WHERE status = :status",
      {
        replacements: { status: "active" },
        type: QueryTypes.SELECT,
      }
    );
  } catch (error) {
    console.error(error);
  }
};

init();
setTimeout(fetchData, 5000);

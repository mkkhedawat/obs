const oracle = require("oracledb");

let connection;

const init = async () => {
  try {
    connection = await oracle.getConnection({
      user: "user",
      password: "password",
      connectString: "localhost:1521/dbName",
    });
    console.log("connected to database");
  } catch (err) {
    console.error(err.message);
  } finally {
    if (connection) {
      try {
        // Always close connections
        await connection.close();
        console.log("close connection success");
      } catch (err) {
        console.error(err.message);
      }
    }
  }
};

init();

const oracle = require("oracledb");
oracle.outFormat = oracle.OUT_FORMAT_OBJECT;

let connection;
const getConnection = async () => {
  if (connection) return connection;
  connection = await oracle.getConnection({
    user: "hr",
    password: "somepwword",
    connectString: "localhost/XEPDB1",
  });
  return connection;
};

const getSomeData = async () => {
  const conn = await getConnection();

  try {
    const result = await conn.execute(
      `SELECT manager_id, department_id, department_name
         FROM departments
         WHERE manager_id = :id`,
      [103] // bind value for :id
    );
    console.log(result.rows);
  } catch (err) {
    console.error(err);
  }
};

const init = () => {
  getConnection();
};

module.exports = { init, getSomeData };

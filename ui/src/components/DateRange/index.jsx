import React, { useState, useRef, forwardRef, useEffect } from "react";
import DatePicker from "react-datepicker";
import toast from "toastr";
import "react-datepicker/dist/react-datepicker.css";
import "./dp-override.css";

import {
  Box,
  InputContainer,
  InputIcon,
  Placeholder,
  DateRangeText,
  Container,
} from "./styles";

const DateRange = forwardRef((props, ref) => {
  const [startDate, setStartDate] = useState(props.startDate || new Date());
  const [endDate, setEndDate] = useState(props.endDate || null);

  const maxAllowedTime = props.maxAllowedDays * 24 * 3600 * 1000;

  const onChange = (dates) => {
    const [start, end] = dates;
    if (end && startDate.getTime() > end.getTime()) {
      setEndDate(null);
      return;
    }

    if (end && end.getTime() - start.getTime() > maxAllowedTime) {
      toast.error("Max date Range allowed is 7 days");
      setEndDate(null);
      return;
    }

    setStartDate(start);
    setEndDate(end);

    if (start && end) {
      console.log(ref);
      ref.current.setOpen(false);
    }
    props.onChange({ start, end });
  };

  const InputPlaceholder = () => (
    <Placeholder>
      <InputIcon />
      <Box>Date Range</Box>
    </Placeholder>
  );

  const DateRangeInput = () => {
    const sInfo = startDate.toDateString().split(" ");
    const eInfo = endDate.toDateString().split(" ");
    return (
      <DateRangeText>{`${sInfo[1]}  ${sInfo[2]}    -    ${eInfo[1]}  ${eInfo[2]}`}</DateRangeText>
    );
  };

  const Input = ({ value, onClick }) => (
    <InputContainer onClick={onClick}>
      {!startDate || !endDate ? (
        <InputPlaceholder onClick={onClick} />
      ) : (
        <DateRangeInput />
      )}
    </InputContainer>
  );

  useEffect(() => {
    setStartDate(props.startDate);
    setEndDate(props.endDate);
  }, [props.lastPropsSync]);

  useEffect(() => {
    props.onChange({ start: startDate, end: endDate });
  }, []);

  return (
    <Container className="CustomDateRange">
      <DatePicker
        ref={ref}
        selected={startDate}
        onChange={onChange}
        startDate={startDate}
        endDate={endDate}
        selectsRange
        disabledKeyboardNavigation
        shouldCloseOnSelect={false}
        customInput={<Input />}
        formatWeekDay={(dayOfWeekLabel) => dayOfWeekLabel.substring(0, 1)}
      />
    </Container>
  );
});
export default DateRange;

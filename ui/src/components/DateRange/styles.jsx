import { styled } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import DateRangeIcon from "@material-ui/icons/DateRange";

const Container = styled(Box)({
  height: 35,
  width: "100%",
  backgroundColor: "#fff",
  color: "rgba(0,0,0,0.77)",
});

const InputContainer = styled(Box)({
  height: 35,
  width: "100%",
  backgroundColor: "#fff",
});

const Placeholder = styled(Box)({
  width: "100%",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  lineHeight: "35px",
});

const InputIcon = styled(DateRangeIcon)({
  marginRight: 15,
});

const DateRangeText = styled(Box)({
  textAlign: "center",
  lineHeight: "35px",
});

export {
  Container,
  InputContainer,
  InputIcon,
  Placeholder,
  DateRangeText,
  Box,
};

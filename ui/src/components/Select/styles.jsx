import { styled } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Select from "react-select";

const ReactSelectStyles = {
  menu: (provided, state) => ({
    ...provided,
    // width: state.selectProps.width,
    borderBottom: "none",
    color: state.selectProps.menuColor,
    marginTop: 0,
    marginBottom: 0,
  }),
  menuList: (provided, state) => ({
    ...provided,
    padding: 0,
  }),
  option: (provided, state) => ({
    ...provided,
    borderBottom: "1px solid rgba(0,0,0,0.2)",
    color: state.isSelected ? "#fff" : "rgba(0,0,0,0.77)",
    backgroundColor: state.isSelected ? "#039869de" : "white",
    ":hover": {
      backgroundColor: "#039869de",
      color: "#fff",
    },
  }),
  control: (provided) => ({
    // none of react-select's styles are passed to <Control />
    ...provided,
    border: "none !important",
    boxShadow: "none",
    marginTop: -10,
    height: 35,
  }),
  indicatorSeparator: (provided) => ({
    visibility: "hidden",
    ...provided,
  }),
  singleValue: (provided, state) => {
    const opacity = state.isDisabled ? 0.5 : 1;
    const transition = "opacity 300ms";
    return {
      ...provided,
      opacity,
      transition,
      color: "rgba(0,0,0,0.77)",
    };
  },
};

const ReactSelect = styled(Select)({
  zIndex: 1000,
  backgroundColor: "#fff",
  border: "none !important",
});

export { ReactSelect, ReactSelectStyles, Box };

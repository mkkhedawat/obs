import React, { useState, useEffect, useRef } from "react";
import DTable, { ExpandButton } from "mui-datatables";
import fileAPIs from "../../apis/FileObserver/file";
import Pagination from "../Pagination";
import { MuiThemeProvider } from "@material-ui/core/styles";
import getMuiTheme from "./theme";

import TableRow from "@material-ui/core/TableRow";
import TableHead from "@material-ui/core/TableHead";
import TableCell from "@material-ui/core/TableCell";

import {
  faArrowAltCircleRight,
  faSortUp,
  faSortDown,
} from "@fortawesome/free-solid-svg-icons";

import {
  TableContainer,
  Box,
  Main,
  TableHeadCell,
  TableIconCell,
  IconContainer,
} from "./styles";

const columnWithSortIcon = (columnMeta, handleToggleColumn, sortOrder) => {
  let icon;
  if (columnMeta.name === sortOrder.name) {
    icon = sortOrder.direction === "desc" ? faSortUp : faSortDown;
  }

  return (
    <TableHeadCell
      key={columnMeta.index}
      onClick={() => handleToggleColumn(columnMeta.index)}
      style={{ cursor: "pointer" }}
    >
      {columnMeta.name}
      <IconContainer icon={icon} />
    </TableHeadCell>
  );
};

const columnWithEndIcon = (columnMeta, handleToggleColumn, sortOrder) => {
  return (
    <TableIconCell key={columnMeta.index} style={{ cursor: "pointer" }}>
      <IconContainer icon={faArrowAltCircleRight} />
    </TableIconCell>
  );
};

const getTableOptions = ({ paginationRef }) => ({
  // filter: true,
  // filterType: "dropdown",
  responsive: "standard",
  expandableRows: false,
  expandableRowsHeader: false,
  expandableRowsOnClick: true,
  elevation: 0,
  export: false,
  print: false,
  search: true,
  download: false,
  filter: true,
  viewColumns: true,
  // sort: true,
  selectableRows: true,
  onFilterChange: () => {
    console.log("cha cha", paginationRef);
    paginationRef.current && paginationRef.current.refreshPagination();
  },
  // customToolbar: () => <div />,
  // customToolbarSelect: () => <div />,
  // searchOpen: true,
  //   isRowExpandable: (dataIndex, expandedRows) => {
  //     if (dataIndex === 3 || dataIndex === 4) return false;

  //     // Prevent expand/collapse of any row if there are 4 rows expanded already (but allow those already expanded to be collapsed)
  //     if (
  //       expandedRows.data.length > 4 &&
  //       expandedRows.data.filter((d) => d.dataIndex === dataIndex).length === 0
  //     )
  //       return false;
  //     return true;
  //   },
  // rowsExpanded: [0, 1],
  // renderExpandableRow: (rowData, rowMeta) => {
  //   return [
  //     <ExpandTableHead className="expand-content">
  //       <ExpandTableCell colSpan={1}></ExpandTableCell>
  //       {rowData.map((d) => (
  //         <ExpandTableCell colSpan={1}>{d}</ExpandTableCell>
  //       ))}
  //     </ExpandTableHead>,
  //     [1, 2, 3].map(() => {
  //       return (
  //         <ExpandTableRow className="expand-content">
  //           <ExpandTableCell colSpan={1}></ExpandTableCell>
  //           <ExpandTableCell colSpan={1}></ExpandTableCell>
  //           <ExpandTableCell colSpan={1}></ExpandTableCell>
  //           {rowData.map((d) => (
  //             <ExpandTableCell colSpan={1}>{d}</ExpandTableCell>
  //           ))}
  //         </ExpandTableRow>
  //       );
  //     }),
  //   ];
  // },
  //   renderExpandableRow: (rowData, rowMeta, a) => {
  //     return (
  //       <TableRow>
  //         <TableCell colSpan={1} />
  //         <TableCell colSpan={rowData.length - 1}>
  //           <Table
  //             className="expandFileDataTable"
  //             title={"Employee list"}
  //             data={[
  //               {
  //                 id: "ABC123",
  //                 date: "2020-06-24T06:36:46.728Z",
  //                 type: "Fire",
  //                 description: "Hot",
  //                 instructions: "",
  //               },
  //               {
  //                 id: "DEF456",
  //                 date: "2020-06-24T06:36:46.728Z",
  //                 type: "Water",
  //                 description: "Wet",
  //                 instructions: "",
  //               },
  //             ]}
  //             columns={[
  //               {
  //                 name: "id",
  //                 label: "ID",
  //               },
  //               {
  //                 name: "type",
  //                 label: "Type",
  //               },
  //               {
  //                 name: "description",
  //                 label: "Description",
  //               },
  //               {
  //                 name: "date",
  //                 label: "Date",
  //               },
  //             ]}
  //             options={{
  //               download: false,
  //               filter: false,
  //               pagination: false,
  //               print: false,
  //               search: false,
  //               viewColumns: false,
  //               expandableRowsHeader: false,
  //               elevation: 0,
  //               selectableRows: "none",
  //               selectToolbarPlacement: "none",
  //               fixedSelectColumn: false,
  //             }}
  //             components={components}
  //           />
  //         </TableCell>
  //       </TableRow>
  //     );
  //   },
  onRowExpansionChange: (curExpanded, allExpanded, rowsExpanded) =>
    console.log(curExpanded, allExpanded, rowsExpanded),
});

const components = {
  ExpandButton: function (props) {
    if (props.dataIndex === 3 || props.dataIndex === 4)
      return <div style={{ width: "24px" }} />;
    return <ExpandButton {...props} />;
  },
};

const parseColumns = (columns) => {
  const temp = columns.map((c) => {
    const options = {
      sortThirdClickReset: true,
      customHeadRender: columnWithSortIcon,
      ...c.options,
    };

    return {
      name: c.name,
      options,
    };
  });
  temp.push({
    name: "",
    options: {
      customBodyRender: columnWithEndIcon,
    },
  });
  return temp;
};

const Table = (props) => {
  const tableRef = useRef(null);
  const paginationRef = useRef(null);

  const columns = parseColumns(props.columns);

  const tableData = (props.data || []).map((row) => [...row, "icon-right"]);

  useEffect(() => {}, []);

  const fetchFileDataFromServer = async () => {
    // setIsFileDataLoading(true);
    // const fileData = await fileAPIs.fetchData();
    // dispatch(updateFileData(fileData));
    // setTimeout(() => {
    //   setIsFileDataLoading(false);
    // }, 600);
  };

  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <Main>
        <TableContainer>
          <DTable
            className="fileDataTable"
            ref={tableRef}
            title={props.title}
            data={tableData}
            columns={columns}
            options={getTableOptions({ paginationRef })}
            components={components}
          />
        </TableContainer>
        <Pagination tableRef={tableRef} ref={paginationRef} />
      </Main>
    </MuiThemeProvider>
  );
};

export default Table;

import { createMuiTheme } from "@material-ui/core/styles";

const getMuiTheme = () =>
  createMuiTheme({
    overrides: {
      //   MUIDataTableHeadRow: {
      //     root: {
      //       backgroundColor: "#0a0a46",
      //       color: "#fff"
      //     },
      //   },
      MUIDataTableHeadCell: {
        root: {
          backgroundColor: "#027784",
          color: "#fff",
        },
        fixedHeader: {
          backgroundColor: "#027784",
          color: "#fff",
        },
        sortActive: {
          color: "#fff",
          backgroundColor: "#027784",
        },
      },
      MUIDataTableToolbar: {
        root: { display: "none" },
      },
      MUIDataTableSelectCell: {
        headerCell: {
          backgroundColor: "#eadadade",
        },
        fixedLeft: {
          background: "#eadadade",
        },
        checked: {
          color: "#027784 !important",
        },
      },
      MUIDataTableHead: {
        main: {
          borderBottom: "10px solid #eadadade",
        },
      },
      MUIDataTableBodyRow: {
        root: {
          borderTop: "5px solid #eadadade",
          background: "#fff",
          "&:hover": {
            background: "#a6e89ede !important",
            color: "#fff",
          },
        },
        // hover: {
        //   background: "red",
        //   color: "red",
        // },
      },
      MUIDataTableFooter: {
        root: {
          display: "none",
        },
      },
      MUIDataTableToolbarSelect: {
        root: {
          display: "none",
        },
      },
      //   MUIDataTable: {
      //     paper: {
      //       padding: "0 10px",
      //     },
      //   },
      MuiButton: {
        label: {
          textTransform: "none",
        },
      },
      MuiFormControl: {
        root: {
          width: "100%",
        },
      },
      MuiTableCell: {
        body: {
          //   color: "green",
          //   background: "#fff",
        },
      },
    },
  });

export default getMuiTheme;

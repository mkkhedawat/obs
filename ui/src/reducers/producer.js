const producer = (state = null, action) => {
  switch (action.type) {
    case "UPDATE_PRODUCER":
      return action.payload;
    default:
      return state;
  }
};

const producerOptions = (state = [], action) => {
  switch (action.type) {
    case "UPDATE_PRODUCER_OPTIONS":
      return action.payload;
    default:
      return state;
  }
};

const selectProducer = (state) => state.producer;
const selectProducerOptions = (state) => state.producerOptions;

export { producer, producerOptions, selectProducer, selectProducerOptions };

const fileData = (state = [], action) => {
  switch (action.type) {
    case "UPDATE_FILE_DATA":
      return action.payload;
    default:
      return state;
  }
};

const selectFileData = (state) => state.fileData;

export { fileData, selectFileData };

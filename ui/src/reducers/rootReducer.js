import { createStore, applyMiddleware, combineReducers } from "redux";
import thunkMiddleware from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";
import { producer, producerOptions } from "./producer";
import { fileMask, fileMaskOptions } from "./fileMask";
import { fileData } from "./fileData";
import { adminFileData } from "./adminFileData";

const rootReducer = combineReducers({
  producer,
  producerOptions,
  fileMask,
  fileMaskOptions,
  fileData,
  adminFileData
});

const composedEnhancer = composeWithDevTools(applyMiddleware(thunkMiddleware));
const store = createStore(rootReducer, composedEnhancer);

export default store;

const fileMask = (state = null, action) => {
  switch (action.type) {
    case "UPDATE_FILE_MASK":
      return action.payload;
    default:
      return state;
  }
};

const fileMaskOptions = (state = [], action) => {
  switch (action.type) {
    case "UPDATE_FILE_MASK_OPTIONS":
      return action.payload;
    default:
      return state;
  }
};

const selectFileMaskOptions = (state) => state.fileMaskOptions;
const selectFileMask = (state) => state.fileMask;

export { fileMask, selectFileMask, fileMaskOptions, selectFileMaskOptions };


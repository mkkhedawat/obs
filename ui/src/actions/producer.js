import filtersAPIs from "../apis/FileObserver/filters";

const updateProducer = (payload) => {
  console.log(payload);
  return {
    type: "UPDATE_PRODUCER",
    payload,
  };
};

const updateProducerOptions = (payload) => {
  console.log(payload);
  return {
    type: "UPDATE_PRODUCER_OPTIONS",
    payload,
  };
};

// const fetchProducerOptions = (dispatch) => {
//   console.log("fetchProducerOptions", dispatch);
//   return async (dispatch) => {
//     // dispatch(itemsIsLoading(true));
//     try {
//       console.log("fetchProducerOptions - await producerOptions", dispatch);
//       const producerOptions = await filtersAPIs.fetchProducerOptions();
//       console.log("fetchProducerOptions - await producerOptions", dispatch);

//       dispatch(updateProducerOptions(producerOptions));
//     } catch (e) {
//       console.log("fetchProducerOptions - error", dispatch);
//       console.error(e);
//     }
//   };
// };

async function fetchProducerOptions(dispatch, getState) {
  const producerOptions = await filtersAPIs.fetchProducerOptions();
  dispatch(updateProducerOptions(producerOptions));
}

export { updateProducer, updateProducerOptions, fetchProducerOptions };

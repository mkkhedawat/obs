const updateFileMask = (payload) => {
  console.log(payload);
  return {
    type: "UPDATE_FILE_MASK",
    payload,
  };
};

const updateFileMaskOptions = (payload) => {
  console.log(payload);
  return {
    type: "UPDATE_FILE_MASK_OPTIONS",
    payload,
  };
};

export { updateFileMask, updateFileMaskOptions };

import React from "react";
import Container from "@material-ui/core/Container";
import { TileContainer, Box } from "./styles";


import Tile from "../../components/Tile";

const Home = () => (
  <Box>
    <Container maxWidth="md">
      <TileContainer>
        <Tile upperText="I am upper" lowerText="I am lower"></Tile>
        <Tile upperText="I am upper" lowerText="I am lower"></Tile>
        <Tile upperText="I am upper" lowerText="I am lower"></Tile>
      </TileContainer>
    </Container>
  </Box>
);

export default Home;

import { styled } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";

const TileContainer = styled(Box)({
  marginTop: "20vh",
  display: "flex",
});

export { TileContainer, Box };

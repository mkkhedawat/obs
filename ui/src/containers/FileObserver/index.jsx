import React, { useState, useEffect, useRef } from "react";
import { useSelector, useDispatch } from "react-redux";
import Table, { ExpandButton } from "mui-datatables";
import fileAPIs from "../../apis/FileObserver/file";
import filtersAPIs from "../../apis/FileObserver/filters";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import SaveIcon from "@material-ui/icons/Save";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import Select from "../../components/Select";
import DateRange from "../../components/DateRange";
import Pagination from "../../components/Pagination";
import InputLabel from "@material-ui/core/InputLabel";
import { selectProducer, selectProducerOptions } from "../../reducers/producer";
import { selectFileMask, selectFileMaskOptions } from "../../reducers/fileMask";
import { selectFileData } from "../../reducers/fileData";
import toast from "toastr";
import CircularProgress from "@material-ui/core/CircularProgress";

import TableRow from "@material-ui/core/TableRow";
import TableHead from "@material-ui/core/TableHead";
import TableCell from "@material-ui/core/TableCell";

import {
  faSort,
  faSortUp,
  faSortDown,
} from "@fortawesome/free-solid-svg-icons";

import {
  updateProducer,
  updateFileMask,
  fetchProducerOptions,
  updateFileMaskOptions,
  updateFileData,
} from "../../actions";

import {
  TableContainer,
  Box,
  Main,
  ExportButton,
  SubmitButton,
  ResetButton,
  FormElementContainer,
  ExpandTableRow,
  ExpandTableHead,
  ExpandTableCell,
  SLAStatusText,
  ExpandTable,
  ProgressContainer,
  TableHeadCell,
  IconContainer,
} from "./styles";

const columnWithSortIcon = (columnMeta, handleToggleColumn, sortOrder) => {
  console.log(columnMeta);
  console.log(handleToggleColumn);
  console.log("sortOrder", sortOrder);
  let icon = faSort;
  if (columnMeta.name === sortOrder.name) {
    icon = sortOrder.direction === "desc" ? faSortUp : faSortDown;
  }

  return (
    <TableHeadCell
      key={columnMeta.index}
      onClick={() => handleToggleColumn(columnMeta.index)}
      style={{ cursor: "pointer" }}
    >
      {columnMeta.name}
      <IconContainer icon={icon} />
    </TableHeadCell>
  );
};

const columns = [
  {
    name: "Name",
    options: {
      filter: true,
      sort: true,
      sortThirdClickReset: true,
      customHeadRender: columnWithSortIcon,
    },
  },
  {
    name: "Title",
    options: {
      filter: true,
      customBodyRender: (value, tableMeta, updateValue) => {
        let color = "#fff";
        let background = "#027784";
        if (value.toLowerCase() === "business analyst") {
          color = "#027784";
          background = "#fff";
        }
        return (
          <SLAStatusText color={color} background={background}>
            value
          </SLAStatusText>
        );
      },
    },
  },
  {
    name: "Location",
    options: {
      filter: false,
    },
  },
  {
    name: "Age",
    options: {
      filter: true,
      sort: true,
      customHeadRender: columnWithSortIcon,
    },
  },
  {
    name: "Salary",
    options: {
      filter: true,
      sort: false,
    },
  },
];

const getTableOptions = ({ paginationRef }) => ({
  // filter: true,
  // filterType: "dropdown",
  responsive: "standard",
  expandableRows: true,
  expandableRowsHeader: false,
  expandableRowsOnClick: true,
  elevation: 0,
  export: false,
  print: false,
  search: true,
  download: false,
  filter: true,
  viewColumns: true,
  // sort: true,
  selectableRows: "none",
  onFilterChange: () => {
    console.log("cha cha", paginationRef);
    paginationRef.current && paginationRef.current.refreshPagination();
  },
  // customToolbar: () => <div />,
  // customToolbarSelect: () => <div />,
  // searchOpen: true,
  isRowExpandable: (dataIndex, expandedRows) => {
    if (dataIndex === 3 || dataIndex === 4) return false;

    // Prevent expand/collapse of any row if there are 4 rows expanded already (but allow those already expanded to be collapsed)
    if (
      expandedRows.data.length > 4 &&
      expandedRows.data.filter((d) => d.dataIndex === dataIndex).length === 0
    )
      return false;
    return true;
  },
  // rowsExpanded: [0, 1],
  // renderExpandableRow: (rowData, rowMeta) => {
  //   return [
  //     <ExpandTableHead className="expand-content">
  //       <ExpandTableCell colSpan={1}></ExpandTableCell>
  //       {rowData.map((d) => (
  //         <ExpandTableCell colSpan={1}>{d}</ExpandTableCell>
  //       ))}
  //     </ExpandTableHead>,
  //     [1, 2, 3].map(() => {
  //       return (
  //         <ExpandTableRow className="expand-content">
  //           <ExpandTableCell colSpan={1}></ExpandTableCell>
  //           <ExpandTableCell colSpan={1}></ExpandTableCell>
  //           <ExpandTableCell colSpan={1}></ExpandTableCell>
  //           {rowData.map((d) => (
  //             <ExpandTableCell colSpan={1}>{d}</ExpandTableCell>
  //           ))}
  //         </ExpandTableRow>
  //       );
  //     }),
  //   ];
  // },
  renderExpandableRow: (rowData, rowMeta, a) => {
    return (
      <TableRow>
        <TableCell colSpan={1} />
        <TableCell colSpan={rowData.length - 1}>
          <Table
            className="expandFileDataTable"
            title={"Employee list"}
            data={[
              {
                id: "ABC123",
                date: "2020-06-24T06:36:46.728Z",
                type: "Fire",
                description: "Hot",
                instructions: "",
              },
              {
                id: "DEF456",
                date: "2020-06-24T06:36:46.728Z",
                type: "Water",
                description: "Wet",
                instructions: "",
              },
            ]}
            columns={[
              {
                name: "id",
                label: "ID",
              },
              {
                name: "type",
                label: "Type",
              },
              {
                name: "description",
                label: "Description",
              },
              {
                name: "date",
                label: "Date",
              },
            ]}
            options={{
              download: false,
              filter: false,
              pagination: false,
              print: false,
              search: false,
              viewColumns: false,
              expandableRowsHeader: false,
              elevation: 0,
              selectableRows: "none",
              selectToolbarPlacement: "none",
              fixedSelectColumn: false,
            }}
            components={components}
          />
        </TableCell>
      </TableRow>
    );
  },
  onRowExpansionChange: (curExpanded, allExpanded, rowsExpanded) =>
    console.log(curExpanded, allExpanded, rowsExpanded),
});

const components = {
  ExpandButton: function (props) {
    if (props.dataIndex === 3 || props.dataIndex === 4)
      return <div style={{ width: "24px" }} />;
    return <ExpandButton {...props} />;
  },
};

const selectOptions = [
  { value: "chocolate", label: "Chocolate" },
  { value: "strawberry", label: "Strawberry" },
  { value: "vanilla", label: "Vanilla" },
];

const FileObserver = () => {
  const [dateRange, setDateRange] = useState({});
  const [isFileDataLoading, setIsFileDataLoading] = useState(false);
  const [lastDateRangePropsSync, setLastDateRangePropsSync] = useState(
    new Date()
  );

  const dispatch = useDispatch();
  const producer = useSelector(selectProducer);
  const producerOptions = useSelector(selectProducerOptions);
  const fileMaskOptions = useSelector(selectFileMaskOptions);
  const fileMask = useSelector(selectFileMask);
  const fileData = useSelector(selectFileData);

  const tableRef = useRef(null);
  const dateRangeRef = useRef(null);
  const paginationRef = useRef(null);

  window.p = paginationRef;

  const today = new Date();
  const yesterday = ((d) => new Date(d.setDate(d.getDate() - 1)))(new Date());

  const handleProducerChange = (data) => {
    dispatch(updateProducer(data));
  };

  const handleFileMaskChange = (data) => {
    dispatch(updateFileMask(data));
  };

  const handleDateRangeChange = (range) => {
    setDateRange(range);
  };

  useEffect(() => {
    fetchFileDataFromServer();
    dispatch(fetchProducerOptions);
  }, []);

  useEffect(() => {
    fetchFileMaskFiltersFromServer();
  }, [producer]);

  const fetchFileDataFromServer = async () => {
    setIsFileDataLoading(true);
    const fileData = await fileAPIs.fetchData();
    dispatch(updateFileData(fileData));
    setTimeout(() => {
      setIsFileDataLoading(false);
    }, 600);
  };

  // const fetchProducerFiltersFromServer = async () => {
  //   dispatch(fetchProducerOptions);
  // };

  const fetchFileMaskFiltersFromServer = async () => {
    dispatch(updateFileMaskOptions([]));
    const fileMaskOptions = await filtersAPIs.fetchFileMaskOptions({
      producer: producer && producer.value,
    });
    dispatch(updateFileMaskOptions(fileMaskOptions));
  };

  const handleReset = () => {
    dispatch(updateProducer(null));
    dispatch(updateFileMask(null));
    setLastDateRangePropsSync(new Date());
  };

  const handleSubmit = () => {
    if (!fileMask) {
      toast.error("File Mask can't be empty");
      return;
    }
    if (!producer) {
      toast.error("Producer can't be empty");
      return;
    }
    if (!dateRange.end || !dateRange.start) {
      toast.error("Invalid date range");
      return;
    }
    toast.success("fetching file data");
    fetchFileDataFromServer();
  };

  const handleExportClick = () => {
    toast.success("handleExportClick");
    // insert code from old project
  };

  return (
    <Main>
      <Grid container spacing={3}>
        <Grid item md={2}>
          <Select
            value={producer}
            placeholder={"producer"}
            options={producerOptions}
            onChange={handleProducerChange}
            isLoading={!(producerOptions && producerOptions.length)}
          ></Select>
        </Grid>
        <Grid item md={2}>
          <Select
            value={fileMask}
            placeholder={"File Mask"}
            options={fileMaskOptions}
            onChange={handleFileMaskChange}
            isLoading={!(fileMaskOptions && fileMaskOptions.length)}
          ></Select>
        </Grid>
        <Grid item md={2}>
          <DateRange
            startDate={yesterday}
            endDate={today}
            maxAllowedDays={7}
            ref={dateRangeRef}
            onChange={handleDateRangeChange}
            lastPropsSync={lastDateRangePropsSync}
          />
        </Grid>
        <Grid item md={1}>
          <SubmitButton
            variant="contained"
            disableElevation
            onClick={handleSubmit}
          >
            Submit
          </SubmitButton>
        </Grid>
        <Grid item md={1}>
          <ResetButton
            variant="contained"
            disableElevation
            onClick={handleReset}
          >
            Reset
          </ResetButton>
        </Grid>
        <Grid item md={3}></Grid>
        <Grid item md={1}>
          <ExportButton
            variant="contained"
            color="primary"
            disableElevation
            startIcon={<SaveIcon />}
            onClick={handleExportClick}
          >
            Export
          </ExportButton>
        </Grid>
      </Grid>
      <TableContainer>
        {!isFileDataLoading ? (
          <Table
            className="fileDataTable"
            ref={tableRef}
            title={"Employee list"}
            data={fileData}
            columns={columns}
            options={getTableOptions({ paginationRef })}
            components={components}
          />
        ) : (
          <ProgressContainer>
            <CircularProgress size={100} />
          </ProgressContainer>
        )}
      </TableContainer>
      <Pagination tableRef={tableRef} ref={paginationRef} />
    </Main>
  );
};

export default FileObserver;

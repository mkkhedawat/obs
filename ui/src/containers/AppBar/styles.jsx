import { styled } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";

const Container = styled(Box)({
  flexGrow: 1,
  height: 60,
  flexGrow: 1,
  display: "flex",
  justifyContent: "flex-end",
  alignItems: "center",
  border: "solid 1px rgba(0,0,0,0.18)",
  paddingRight: 30,
});

const User = styled(Typography)({
  color: "#0800ffc9",
  marginRight: 10,
});

export { Container, User, Box };

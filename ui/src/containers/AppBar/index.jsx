import React from "react";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";

import { Container, User, Box } from "./styles";

const AppBar = () => (
  <Container>
    <User variant="h6">Michael Scott</User>
    <Box>
      <AccountCircleIcon />
    </Box>
  </Container>
);

export default AppBar;

import React, { useState, useEffect, useRef } from "react";
import Container from "@material-ui/core/Container";
import { useSelector, useDispatch } from "react-redux";
import { selectAdminFileData } from "../../reducers/adminFileData";
import { LinkContainer, Box, Button, ButtonContainer } from "./styles";
import Table from "../../components/Table";
import fileAPIs from "../../apis/FileObserver/adminTool";

import { updateAdminFileData } from "../../actions";

const ViewDetailsLink = <LinkContainer href="#">View Details</LinkContainer>;

const placeHolderData = [
  [
    "Gabby George",
    "Business Analyst",
    "Minneapolis",
    30,
    "$100,000",
    ViewDetailsLink,
  ],
  [
    "Aiden Lloyd",
    "Business Consultant",
    "Dallas",
    55,
    "$200,000",
    ViewDetailsLink,
  ],
  ["Jaden Collins", "Attorney", "Santa Ana", 27, "$500,000", ViewDetailsLink],
  [
    "Franky Rees",
    "Business Analyst",
    "St. Petersburg",
    22,
    "$50,000",
    ViewDetailsLink,
  ],
  [
    "Aaren Rose",
    "Business Consultant",
    "Toledo",
    28,
    "$75,000",
    ViewDetailsLink,
  ],
];

const columns = [
  { name: "Producer", id: "active" },
  { name: "Type", id: "comments" },
  { name: "Direction", id: "direction" },
  { name: "Producer", id: "time" },
  { name: "ID", id: "id" },
  { name: "", id: "" },
];

const AdminTool = () => {
  const [isDataLoading, setIsDataLoading] = useState(false);

  const dispatch = useDispatch();
  const data = useSelector(selectAdminFileData);

  const processAdminData = (data) => {
    if (!data || !Array.isArray(data)) return;
    return data.map((d) => {
      const temp = [];
      columns.forEach((c) => {
        if (c.id) {
          temp.push(d[c.id]);
        }
      });
      temp.push(ViewDetailsLink);
      return temp;
    });
  };

  const fetchAdminDataFromServer = async () => {
    setIsDataLoading(true);
    const data = await fileAPIs.fetchData();
    console.log(processAdminData(data));
    dispatch(updateAdminFileData(processAdminData(data)));
    setTimeout(() => {
      setIsDataLoading(false);
    }, 600);
  };

  useEffect(() => {
    fetchAdminDataFromServer();
  }, []);

  console.log(data);

  return (
    <Box>
      <ButtonContainer>
        <Button variant="contained" color="primary" disableElevation>
          Delete
        </Button>
        <Button variant="contained" color="primary" disableElevation>
          Add File
        </Button>
      </ButtonContainer>

      <Table data={data} columns={columns}></Table>
    </Box>
  );
};

export default AdminTool;

import axios from "../../libs/axios";

const placeHolderData = [
  { value: "chocolate", label: "Chocolatetu" },
  { value: "strawberry", label: "yStrawberry" },
  { value: "vanilla", label: "Vanilla" },
];

const placeHolderData2 = [
  { value: "chocolate", label: "c" },
  { value: "strawberry", label: "a" },
  { value: "vanilla", label: "Vanilla" },
];

const fetchProducerOptions = async () => {
  try {
    const { data } = await axios.post("http://someEndpoint", {});
    return data;
  } catch (err) {
    console.error(err && err.message);
  }
  return placeHolderData;
};

const fetchFileMaskOptions = async ({ producer }) => {
  console.log(producer);
  try {
    const { data } = await axios.post("http://someEndpointfm", {});
    return data;
  } catch (err) {
    console.error(err && err.message);
  }
  return placeHolderData2;
};

export default { fetchProducerOptions, fetchFileMaskOptions };

import axios from "../../libs/axios";

const placeHolderData = [
  {
    active: "string",
    comments: "string",
    direction: "string",
    time: "string",
    id: 1,
  },
  {
    active: "string",
    comments: "string",
    direction: "string",
    time: "string",
    id: 2,
  },
];

const fetchData = async () => {
  try {
    const { data } = await axios.post("http://someEndpoint", {});
    return data;
  } catch (err) {
    console.error(err && err.message);
  }
  return placeHolderData;
};

export default { fetchData };

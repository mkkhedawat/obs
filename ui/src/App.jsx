import React from "react";
import { MemoryRouter, Switch, Route } from "react-router-dom";
import Home from "./containers/Home";
import AdminTool from "./containers/AdminTool";
import FileObserver from "./containers/FileObserver";
import AppBar from "./containers/AppBar";
import Box from "@material-ui/core/Box";
import getMuiTheme from "./themes";
import { MuiThemeProvider } from "@material-ui/core/styles";
import { Provider } from "react-redux";
import "toastr/build/toastr.css";
import store from "./reducers/rootReducer";

const Main = () => {
  return (
    <Box className="Main">
      <MuiThemeProvider theme={getMuiTheme()}>
        <Provider store={store}>
          <MemoryRouter initialEntries={["/AdminTool"]} initialIndex={0}>
            <AppBar></AppBar>
            <Switch>
              <Route path={"/home"} component={Home} />
              <Route path={"/AdminTool"} component={AdminTool} />
              <Route path={"/file-observer"} component={FileObserver} />
            </Switch>
          </MemoryRouter>
        </Provider>
      </MuiThemeProvider>
    </Box>
  );
};

export default Main;

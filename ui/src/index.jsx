import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import Base from "@material-ui/core/CssBaseline";
import * as serviceWorker from "./utils/serviceWorker";
import "./styles/index.css";

const Main = (
  <>
    <Base />
    <App />
  </>
);

ReactDOM.render(Main, document.getElementById("root"));
serviceWorker.unregister();

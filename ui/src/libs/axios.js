import axios from "axios";

const axiosInstance = axios.create({
  headers: {},
});

const requestInterceptor = async (config) => {
  // if we need to add anything to outgoing request
  return config;
};

axiosInstance.interceptors.request.use(requestInterceptor);

export default axiosInstance;
